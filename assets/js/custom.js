$(document).ready(function() {

  // Slider CDN
  $('.bxslider').bxSlider({
    controls: false
  });

  // Mobile navigation

  $('.toggle-nav').click(function(e) {
    $(this).toggleClass('active');
    $('.menu ul').toggleClass('active');

    e.preventDefault();
  });

});
